package sheridan;

import sheridan.LiquourType;

import java.util.ArrayList;
import java.util.List;

public class LiquourService {
	

    public List<String> getAvailableBrands(LiquourType type){

        List<String> brands = new ArrayList( );

        if(type.equals(LiquourType.Wine)){
            brands.add("Adrianna Vineyard");
            brands.add(("J. P. Chenet"));
            brands.add("Barefoot");
            brands.add("Spunmante Bambino");
            brands.add("Yellowglen");

        }else if(type.equals(LiquourType.Whiskey)){
            brands.add("Glenfiddich");
            brands.add("Johnnie Walker");
            brands.add("Seagrams");
            brands.add("Buchanan's");
            brands.add("Jack Daniel's");

        }else if(type.equals(LiquourType.Beer)){
            brands.add("Corona");
            brands.add("Budweiser");
            brands.add("Heineken");
            brands.add("Coors");
            brands.add("Canadian");
            brands.add("Guinness");

            
        }else {
            brands.add("No Brand Available");
        }
    return brands;
    }
}
